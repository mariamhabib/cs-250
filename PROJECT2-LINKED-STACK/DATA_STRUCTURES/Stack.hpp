#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"
#include "LinkedList.hpp"

template <typename T>
class LinkedStack
{
    public:
    LinkedStack()
    {
		m_itemCount = 0;

    }

    void Push( const T& newData )
    {
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;
		m_itemCount++;

		// If the list is empty, this is the first item.
		if (m_ptrLast == nullptr)
		{
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}

		// Otherwise, it goes at the end; it is the new last item.
		else
		{
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
		}

    }

    T& Top()
    {

		if (m_ptrLast == nullptr)
		{
			throw out_of_range("Cannot GetBack() for an empty list");
			
		}
		else
		{
			return m_ptrLast->data;
		}
		
		//throw runtime_error("Not yet implemented"); // placeholder
    }

    void Pop()
    {
		if (m_ptrLast == nullptr)
		{
			// Ignore this scenario
			// If statement left to explicitly state what happens.
		}

		// If there is only one item in the list...
		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = m_ptrLast = nullptr;
			m_itemCount--;
		}

		// If there is more than one item in the list...
		else
		{
			// Locate the second-to-last item in the list. It will be the new last.
			Node<T>* ptrSecondToLast = m_ptrLast->ptrPrev;
			// Clearing out the next pointer for the new-last-item.
			ptrSecondToLast->ptrNext = nullptr;
			// Remove the old-last-item
			delete m_ptrLast;
			// Point the last pointer to the new-last-item.
			m_ptrLast = ptrSecondToLast;
			m_itemCount--;
		}

    }

    int Size()
    {
        return m_itemCount;    // placeholder
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
