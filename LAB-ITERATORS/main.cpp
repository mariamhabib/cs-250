#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

#include "Menu.hpp"

list<string> LoadBook( const string& filename );
void ReadBook( list<string> bookText );

int main()
{
    vector<string> books = { "aesop.txt", "fairytales.txt" };

    bool done = false;
    while ( !done )
    {
        Menu::Header( "LIBRARY" );

        cout << "Which book do you want to read?" << endl;
        int choice = Menu::ShowIntMenuWithPrompt( books );

        list<string> bookText = LoadBook( books[choice-1] );
        ReadBook( bookText );
    }

	cin.ignore();
	cin.get();
    return 0;
}


list<string> LoadBook( const string& filename )
{
    list<string> bookText;

    cout << "Loading " << filename << "..." << endl;

    ifstream input( filename );

    if ( !input.good() )
    {
        cout << "Error opening file" << endl;
    }

    string line;
    while ( getline( input, line ) )
    {
        bookText.push_back( line );
    }

    cout << endl << bookText.size() << " lines loaded" << endl << endl;

    input.close();

    return bookText;
}


void ReadBook( list<string> bookText )
{
	int counter = 0;
	int lines = 0;
	int pageLength = 25;
	int totalLines = 0;
	/*int pageHeight = 25;
	int counter = 0;
	int totalLines = 0;
	int line = 0;
	for(list<string>::iterator line = bookText.begin(); line != bookText.end(); )

	{ 
		cout << line << endl;
		counter++;
		totalLines++;
		line++;
	}
	*/
	list<string> ::iterator it;
	for (it = bookText.begin(); it != bookText.end(); )
	{
		cout << *it << endl;
		counter++;
		lines++;
		it++;
	}
	if (counter == pageLength) //filled page
	{
		Menu::DrawHorizontalBar(80);
		cout << "Line" << lines << " of " << bookText.size() << endl << endl;

		int choice = Menu::ShowIntMenuWithPrompt({ "BACKWARDS","FORWARD" }, false);

		if (choice == 1) //previous
		{
			for (int i = 0; i < pageLength * 2; i++)
			{
				if (it != bookText.begin())
				{
					lines--;
					it--;
				}
			}
			totalLines -= pageLength * 2;
		}
		
		counter = 0;
	}
}
