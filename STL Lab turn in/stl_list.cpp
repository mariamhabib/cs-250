// Lab - Standard Template Library - Part 2 - Lists
// Mariam Habib

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states)
{
	for (
		list<string>::iterator it = states.begin(); //initialization code, keep looping while, run after each iteration
		it != states.end();
		it++
		)
	{
		cout << *it << "\t" << endl;
	}
	

}




int main()
{
	list<string> states;

	bool done = false;
	while (!done)
	{
		cout << "1. Push Front "
			<< "2. Push Back "
			<< "3. Pop front "
			<< "4. Pop Back "
			<< "5. Continue " << endl;

		int choice;
		cin >> choice;

		switch (choice)
		{
		case 1:
		{
			cout << "Enter a new state: ";
			string state;
			cin >> state;
			states.push_front(state);
			break;

		case 2:
			cout << "Enter a new state: ";
			string state;
			cin >> state;
			states.push_back(state);
			break;

		case 3:
			cout << "Enter a new state: ";
			string state;
	
			states.pop_front();
			break;

		case 4:
			cout << "Enter a new state: ";
			string state;
			
			states.pop_back();
			break;

		case 5:
			done = true;
			break;
		}
		}
	}
    cin.ignore();
    cin.get();
    return 0;
}
