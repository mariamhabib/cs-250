﻿#ifndef DICTIONARY_HPP
#define DICTIONARY_HPP

#include <string>
#include <iomanip>
#include <iostream>
#include <stdexcept>
using namespace std;

#include "Node.hpp"

//! The maximum size of the array.
const int TABLE_SIZE = 131;

//! An enumeration to stand for which collision method to use
enum CollisionMethod { LINEAR, QUADRATIC, DOUBLE };

//! key-value array using a hash table
template <typename TK, typename TD>
class HashTable
{
    public:
    HashTable();
    void SetCollisionMethod( CollisionMethod cm );
    CollisionMethod GetCollisionMethod();

    bool IsEmpty();
    int Size();
    void Clear();

    void Insert( const TK& key, const TD& data );
    void Remove( const TK& key );
    TD& GetItem( const TK& key );
    bool Contains( const TK& key );

    void Print( const string& filename );

    private:
    int GetUnusedIndex( int key );
    int GetItemIndex( int key );
    int HashFunction( int key );

    int LinearProbe( int key );
    int QuadraticProbe( int key, int& addValue );
    int HashFunction2( int key );

    //! The internal array
    Node<TK,TD> m_data[TABLE_SIZE];

    //! Which collision method is being used
    CollisionMethod m_collisionMethod;

    //! The amount of items being stored in the array
    int m_size;

    friend class Tester;
};

//! Returns true if the table is empty, or false if it is not.
template <typename TK, typename TD>
bool HashTable<TK,TD>::IsEmpty()
{
	if (m_size == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//! Returns the amount of items stored in the table
template <typename TK, typename TD>
int HashTable<TK,TD>::Size()
{
	return m_size;
}

//! Clears out the table
template <typename TK, typename TD>
void HashTable<TK,TD>::Clear()
{
	for (int i = 0; i < TABLE_SIZE; i++)
	{
		m_data[i].hasData = false;
	}
}

//! Get an available index to use for a new item
template <typename TK, typename TD>
int HashTable<TK, TD>::GetUnusedIndex(int key)
{
	int index = HashFunction(key);
	int originalIndex = index;
	int collisions = 0;
	int doubleHashAdd = 0;

	while (m_data[index].hasData)
	{
		collisions++;
		switch (m_collisionMethod)
		{
		case LINEAR:
			index = LinearProbe(index) % TABLE_SIZE;
			break;
		case QUADRATIC:
			index = QuadraticProbe(originalIndex, collisions) % TABLE_SIZE;
			break;
		case DOUBLE:
			doubleHashAdd = collisions * HashFunction2(originalIndex);
			index = (originalIndex + doubleHashAdd) % TABLE_SIZE;
			break;
		}
	}
	return index;
}

	

//! Get the index of an existing item, given some key.
template <typename TK, typename TD>
int HashTable<TK, TD>::GetItemIndex(int key)
{
	int index = HashFunction(key);
	int originalIndex = index;
	int quadraticAdd = 1;
	int collisionCount = 0;
	int addAmount = 0;
	int doubleHashAdd = 0;

	while (m_data[index].hasData && m_data[index].key != key)
	{
		collisionCount++;
		switch (m_collisionMethod)
		{
		case LINEAR:
			index = LinearProbe(index) % TABLE_SIZE;
			break;
		case QUADRATIC:
			index = QuadraticProbe(originalIndex, collisionCount) % TABLE_SIZE;
			break;
		case DOUBLE:
			doubleHashAdd = collisionCount * HashFunction2(originalIndex);
			index = (originalIndex = doubleHashAdd) % TABLE_SIZE;
			break;

			
		}
	}
	return index;
}
//! Generates an index for the new item using the key and stores it in the array.
template <typename TK, typename TD>
void HashTable<TK,TD>::Insert( const TK& key, const TD& data )
{
	if (m_size == TABLE_SIZE)
	{
		throw runtime_error("CAN'T INSERT -- TABLE IS FULL");
	}

	int index = GetUnusedIndex(key);

	m_data[index].key = key;
	m_data[index].data = data;
	m_data[index].hasData = true;
	m_size++;
}

//! Soft-removes the item at the given key
//Use the GetItemIndex to get the index of the existing item in the array, using the key passed in. 
//If the index is found,
//then set the item's hasData variable to false and decrement the size.
template <typename TK, typename TD>
void HashTable<TK,TD>::Remove( const TK& key )
{
    int index = GetItemIndex(key);
	if (m_data[index].key > 0)
	{
	m_data[index].hasData = false;
	m_size--;
	}
	else
	{
		throw runtime_error("Error: key not found");
	}
}

//! Returns the data for the item, given some key.
//Use the GetItemIndex to find the index of the item (given the key). If the item isn't found, throw an exception.
//Otherwise, if it is found, return the item's data.
template <typename TK, typename TD>
TD& HashTable<TK, TD>::GetItem(const TK& key)
{
	int index = GetItemIndex(key);

	if (m_data[index].key < 0)
	{
		throw runtime_error("Index not found for given key");
	}
	else
	{
		return m_data[index].data;
	}
}

//! Tries to find an item given some key, and returns true if found, false if not.
//Try to find the index of the item given the key. If the item doesn't exist, return false. 
//If it does exist, return true.
template <typename TK, typename TD>
bool HashTable<TK, TD>::Contains(const TK& key)
{
	int index = GetItemIndex(key);

	if (m_data[index].hasData <= 0)
	{
		return false;
	}

		return index;

}

//! Primary hash function
template <typename TK, typename TD>
int HashTable<TK,TD>::HashFunction( int key )
{
	return key % TABLE_SIZE;
}

//! Steps the index forward by one
template <typename TK, typename TD>
int HashTable<TK,TD>::LinearProbe( int index )
{
	return index + 1;
}

//Steps the index forward quadratically
//The addValue is the amount of collisions found.
//• For the first collision, we add + 1∧2 to the index.
//• For the second collision, we add + 2∧2 to the index.
//• For the nth collision, we add + n∧2 to the index.
//So calculate the square of addValue, and add it to the index.Make sure to increment addValue before returning
//from the function.

template <typename TK, typename TD>
int HashTable<TK,TD>::QuadraticProbe( int index, int& addValue )
{
	int quadratic = addValue * addValue;
	return index + quadratic;
}

//! Secondary hash function
//Generate an index by using a hash function with the key.
//Hash function to use :
//• 7 - (key mod 7)
template <typename TK, typename TD>
int HashTable<TK,TD>::HashFunction2( int key )
{
	return 7 - (key % 7);
}

/* ******************************************************************** Functions I've already completed */

//! Initialize the hash table
template <typename TK, typename TD>
HashTable<TK,TD>::HashTable()
{
    m_collisionMethod = LINEAR; // default
    m_size = 0;
}

//! Set the collision method
template <typename TK, typename TD>
void HashTable<TK,TD>::SetCollisionMethod( CollisionMethod cm )
{
    m_collisionMethod = cm;
}

//! Returns the collision method
template <typename TK, typename TD>
CollisionMethod HashTable<TK,TD>::GetCollisionMethod()
{
    return m_collisionMethod;
}

//! Writes out all the set data from the table to a file
template <typename TK, typename TD>
void HashTable<TK,TD>::Print( const string& filename )
{
    ofstream output( filename );
    output << left
        << setw( 10 ) << "INDEX"
        << setw( 10 ) << "KEY"
        << setw( 10 ) << "DATA"
        << endl;

    for ( int i = 0; i < TABLE_SIZE; i++ )
    {
        if ( m_data[i].hasData )
        {
            output << left
                << setw( 10 ) << i
                << setw( 10 ) << m_data[i].key
                << setw( 10 ) << m_data[i].data
                << endl;
        }
        else
        {
            output << left
                << setw( 10 ) << i
                << setw( 10 ) << "--------------------------------------------"
                << endl;
        }
    }
    output.close();
}

#endif
