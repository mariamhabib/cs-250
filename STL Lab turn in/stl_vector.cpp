// Lab - Standard Template Library - Part 1 - Vectors
// Mariam Habib

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector <string> courses; //string[] courses

	bool done = false;
	while (!done)
	{

		cout << "1. Add a new course, "
			<< "2. Remove the last course, "
			<< "3. Display the course list, "
			<< "4. Quit" << endl;

		int choice;
		cin >> choice;

		if (choice == 1)
		{
			string courseName;
			cout << "Please enter the course: ";
			//cin >> courseName;
			cin.ignore();
			getline(cin, courseName);

			courses.push_back(courseName); //add to list of courses
		
		}
		else if (choice == 2)
		{
			courses.pop_back(); //remove last item in list of courses
		}
		else if (choice == 3)
		{
			for ( unsigned int i = 0; i < courses.size(); i++)
			{
				cout << i << ". " << courses[i] << endl;
			}
				
		}
		else if (choice == 4)
		{
			done = true;
		}
	}
    cin.ignore();
    cin.get();
    return 0;
}
