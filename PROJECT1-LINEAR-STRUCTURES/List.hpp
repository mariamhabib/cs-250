#ifndef _LIST_HPP
#define _LIST_HPP

#include <iostream>
using namespace std;

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount;
	T m_arr[ARRAY_SIZE];            // Add this!

									// functions for interal-workings
	bool ShiftRight(int atIndex)
	{
		for (int i = m_itemCount; i > atIndex; i--)
		{
			m_arr[i] = m_arr[i - 1];
		}

	}

	bool ShiftLeft(int atIndex)
	{
		for (int i = atIndex; i < m_itemCount; i++)
		{
			m_arr[i] = m_arr[i + 1];
		}
	}

public:
	List()
	{
		m_itemCount = 0;
	}

	~List()
	{
	}

	// Core functionality
	int     Size() const
	{
		return m_itemCount;
	}

	bool    IsEmpty() const
	{
		return (m_itemCount == 0);
	}

	bool    IsFull() const
	{
		return (m_itemCount == ARRAY_SIZE);
	}

	bool    PushFront(const T& newItem)
	{
		if (IsFull())
		{
			throw runtime_error("Array is full");
		}
		ShiftRight(0);
		m_arr[0] = newItem;
		m_itemCount++;
	}

	bool  PushBack(const T& newItem)
	{

		if (IsFull())
		{
			throw runtime_error("Array is full");
		}


		m_arr[m_itemCount] = newItem;
		m_itemCount++;
	
	}

	bool   Insert(int atIndex, const T& item)
	{
		if (IsFull())
		{
			throw runtime_error("Array is full");
		}
		else if (atIndex < 0 || atIndex > m_itemCount)
		{
			throw out_of_range("Invalid index!");
		}
		else if (atIndex == 0)
		{
			PushFront(item);
		}
		else if (atIndex == m_itemCount)
		{
			PushBack(item);
		}
		else
		{
			ShiftRight(atIndex);
			m_arr[atIndex] = item;
			m_itemCount++;

		}
	}

	bool    PopFront()
	{
		if (m_itemCount > 0)
		{
			ShiftLeft(0);
			m_itemCount--;
		}
		
	}

	bool    PopBack()
	{
		if (m_itemCount > 0)
		{
			m_itemCount--;
		}
	
	}

	bool    Remove(const T& item)
	{
		for (int i = m_itemCount - 1; i >= 0; i--)
		{
			if (m_arr[i] == item)
			{
				ShiftLeft(i);
				m_itemCount--;
			}
		}
	}

	bool   Remove(int atIndex)
	{
		if (atIndex < 0 || atIndex >= m_itemCount)
		{
			throw out_of_range("Invalid index!");
		}
	}

	bool    Clear()
	{
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (atIndex < 0 || atIndex >= m_itemCount  )
		{
			return nullptr;
		}
		return &m_arr[atIndex];
	}

	T*      GetFront()
	{
		if (m_itemCount == 0)
		{
			return nullptr;
		}
		return &m_arr[0];
	}

	T*      GetBack()
	{
		if (m_itemCount == 0)
		{
			return nullptr;
		}
		return &m_arr[m_itemCount - 1];
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		int counter = 0;
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
			{
				counter++;
			}
		}
		return counter;
	
	}

	bool    Contains(const T& item) const
	{
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] == item)
			{
				return true;
			}
		}
		return false;
	}

	friend class Tester;
};


#endif
