#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe(vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile)
{
	ofstream output(logFile);
	output << "First come First Served (FCFS) " << endl;

	int cycles = 0;


	while (jobQueue.Size() > 0)
	{

		output << "CYCLE: " << cycles << "\t";
		jobQueue.Front()->Work(FCFS); //process the front most item
		output << "REMAINING" << jobQueue.Front()->fcfs_timeRemaining << endl;

		if (jobQueue.Front()->fcfs_done)
		{
			jobQueue.Front()-> SetFinishTime(cycles, FCFS); //set front most item
			jobQueue.Pop(); //pop the item off
		}

		cycles++;
	}
	output.close();

	output << "Summary: " << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << "Job ID: " << allJobs[i].id
			<< "Time to complete: " << allJobs[i].rr_finishTime
			<< "Times interrupted: " << allJobs[i].rr_timesInterrupted
			<< endl;
	}

}
void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Red Robin " << endl;
	
	int cycles = 0;
	int timer = 0;

	while (jobQueue.Size() > 0)
	{
		output << jobQueue.Front()->id << endl;

		if (timer == timePerProcess)
		{
			jobQueue.Front()->rr_timesInterrupted++;

			jobQueue.Push(jobQueue.Front());

			jobQueue.Pop();

			timer = 0;
		}

		jobQueue.Front()->Work(RR);//process the front most item
		
		if (jobQueue.Front()->rr_done)
		{
			jobQueue.Front()->SetFinishTime(cycles, RR);//set front most item via set finish time
			jobQueue.Pop(); //pop item off queue
		}
		cycles++;
		timer++;
	}

	output.close();

	output << "Summary: " << endl;
	for (unsigned int i = 0; i < allJobs.size(); i++)
	{
		output << "Job ID: " << allJobs[i].id
			<< "Time to complete: " << allJobs[i].rr_finishTime
			<< "Times interrupted: " << allJobs[i].rr_timesInterrupted
			<< endl;
	}
}

#endif
